//write your code here
#include <stdio.h>
int main()
{
	int n, num, found = 0;
	printf("Enter the number of elements in the array ");
	scanf("%d", &n);
	int ar[n], beg = 0, end = n - 1;
	printf("Enter the elements in ascending order\n");
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &ar[i]);
	}
	printf("Enter the number to be searched ");
	scanf("%d", &num);
	while (beg < end)
	{
		int mid = (beg + end) / 2;
		if (ar[mid] == num)
		{
			printf("%d is found at position %d", num, mid);
			found = 1;
			break;
		}
		else if (ar[mid] < num)
			beg = mid + 1;
		else
			end = mid - 1;
	}
	if ((beg > end) || (found ==0))
		printf("Number not found");
	return 0;
}