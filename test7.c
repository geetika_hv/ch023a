//write your code here
#include <stdio.h>
int main()
{
	struct stud
	{
		char name[20];
		int rno;
		char course[20];
		int sem;
		int marks;
	} stud1, stud2;
	printf("Enter name, roll number, course, semester and total marks of student 1\n");
	scanf("%s%d%s%d%d", stud1.name, &stud1.rno,stud1.course,&stud1.sem, &stud1.marks);
	printf("Enter name, roll number, course, semester and total marks of student 2\n");
	scanf("%s%d%s%d%d", stud2.name, &stud2.rno,stud2.course,&stud2.sem, &stud2.marks);
	printf("Details of student 1 are\n");
	printf("Name: %s\n", stud1.name);
	printf("Roll no: %d\n", stud1.rno);
	printf("Course: %s\n", stud1.course);
	printf("Semester: %d\n", stud1.sem);
	printf("Marks: %d\n", stud1.marks);
	printf("Details of student 2 are\n");
	printf("Name: %s\n", stud2.name);
	printf("Roll no: %d\n", stud2.rno);
	printf("Course: %s\n", stud2.course);
	printf("Semester: %d\n", stud2.sem);
	printf("Marks: %d\n", stud2.marks);
	if (stud1.marks > stud2.marks)
		printf("%s has scored highest marks", stud1.name);
	else
		printf("%s has scored highest marks", stud2.name);
	return 0;
}