#include<stdio.h>
int main()
{
	int marks[3][5],i,j,s,sub;
	printf("Enter the marks of 5 students in 3 subjects\n");
	for(i=0;i<5;i++)
	{
		for(j=0;j<3;j++)
		{
			scanf("%d",&marks[i][j]);
		}
	}
	printf("Marks:\n");
	for(i=0;i<5;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("%d\t",marks[i][j]);
		}
		printf("\n");
	}
	for(i=0;i<3;i++)
	{
		int h=-1;
		for(j=0;j<5;j++)
		{
			if(h<marks[j][i])
			{
			   h=marks[j][i];
			   s=j;
			   sub=i;
			}
		}
		printf("%d is the highest mark in subject %d scored by student %d\n",h,sub,s);
	}
	return 0;
}
