//write your code here
#include <stdio.h>
#include <string.h>
int main()
{
	char str[50], original[50];
	int temp, i = 0, j = 0, p = 0;
	printf("Enter the string ");
	gets(str);
	while (str[i] != '\0')
	{
		original[i] = str[i];
		i++;
	}
	int len = strlen(str) - 1;
	int m = 0, n = len;
	while (m < n)
	{
		temp = str[m];
		str[m] = str[n];
		str[n] = temp;
		m++;
		n--;
	}
	printf("Reversed string: ");
	puts(str);
	while (j < len)
	{
		if (str[j] == original[j])
			j++;
		else
			p = 1;
		break;
	}
	if (p == 1)
		printf("Not a palindrome");
	else
		printf("Palindrome");
	return 0;
}