//write your code here
#include <stdio.h>
int main()
{
	struct employee
	{
		int emp_id;
		struct name
		{
			char first[10];
			char last[10];
		} n;
		struct date
		{
			int dd, mm, yy;
		} doj;
		int salary;
	} emp;
	printf("Enter employee id: ");
	scanf("%d", &emp.emp_id);
	printf("Enter name: ");
	scanf("%s%s", emp.n.first, emp.n.last);
	printf("Enter date of joining: ");
	scanf("%d%d%d", &emp.doj.dd, &emp.doj.mm, &emp.doj.yy);
	printf("Enter salary: ");
	scanf("%d", &emp.salary);
	printf("Employee ID:%d\n", emp.emp_id);
	printf("Name: %s %s\n", emp.n.first, emp.n.last);
	printf("Date of joining: %d-%d-%d\n", emp.doj.dd, emp.doj.mm, emp.doj.yy);
	printf("Salary: %d", emp.salary);
	return 0;
}