#include<stdio.h>
int swap(int *p,int *q)
{
	int t=*p;
	*p=*q;
	*q=t;
}
int main()
{
	int n1,n2,*p,*q;
	printf("Enter two numbers ");
	scanf("%d%d",&n1,&n2);
	printf("Before swapping: num1=%d  num2=%d\n",n1,n2);
	p=&n1;
	q=&n2;
	swap(&n1,&n2);
	printf("After swapping: num1=%d  num2=%d",*p,*q);
	return 0;
}