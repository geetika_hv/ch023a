//write your code here
#include<stdio.h>
int main()
{
	int r,c;
	printf("Enter the number of rows and columns");
    scanf("%d%d",&r,&c);
    int ar[r][c];
    printf("Enter the elements of the array ");
    for(int i=0;i<r;i++)
    {
    	for(int j=0;j<c;j++)
    	{
    		scanf("%d",&ar[i][j]);
    	}
    }
    printf("Matrix:\n");
    for(int i=0;i<r;i++)
    {
    	for(int j=0;j<c;j++)
    	{
    		printf("%d\t",ar[i][j]);
    	}
    	printf("\n");
    }
    printf("Transpose of the matrix:\n");
    for(int i=0;i<c;i++)
    {
    	for(int j=0;j<r;j++)
    	{
    		printf("%d\t",ar[j][i]);
    	}
    	printf("\n");
    }
    return 0;
}
