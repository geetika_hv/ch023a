#include <stdio.h>
int main()
{
	int r1, c1, r2, c2;
	printf("Enter the number of rows and columns in first matrix");
	scanf("%d%d", &r1, &c1);
	printf("Enter the number of rows and columns in second matrix");
	scanf("%d%d", &r2, &c2);
	int a[r1][c1], b[r2][c2], c[r1][c1];
	printf("Enter the elements of first matrix\n");
	for (int i = 0; i < r1; i++)
	{
		for (int j = 0; j < c1; j++)
		{
			scanf("%d", &a[i][j]);
		}
	}
	printf("Enter the elements of second matrix\n");
	for (int i = 0; i < r2; i++)
	{
		for (int j = 0; j < c2; j++)
		{
			scanf("%d", &b[i][j]);
		}
	}
	if ((r1 != r2) || (c1 != c2))
		printf("Addition not possible");
	else
	{
		printf("Resultant matrix:\n");
	for (int i = 0; i < r1; i++)
	{
		for (int j = 0; j < c1; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
			printf("%d\t", c[i][j]);
		}
		printf("\n");
	}
	}
	return 0;
}
